package graphic_client;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cs_2017_tp7_api.IApplication;
import cs_2017_tp7_api.IClientControler;
import cs_2017_tp7_api.IClientView;
import cs_2017_tp7_api.IConstants;
import cs_2017_tp7_api.ISessionClient;
import cs_2017_tp7_api.IShip;
import cs_2017_tp7_common.Controler;
import cs_2017_tp7_common.Ship;

public class ClientControler extends Controler implements IClientControler{
	
	private ISessionClient client ;
	private IClientView clientView;
	private int[] id;
	private Random rand = new Random();
	int shoot_x = 0;
	int shoot_y = 0;
	
	public ClientControler(IApplication application) {
		super(application);
	}

	@Override
	public boolean connect(String[] ip) {
		localHost = ip[0];
		localPort = Integer.parseInt(ip[1]);
		if (client.connect(localHost, localPort)) {
			view.notifyConnected(true);
			return true;
		}
		return false;
	}

	@Override
	public void cmd(String cmd) {
		client.sendCommand(cmd);	
	}

	@Override
	public void setNetwork(Object  networkObject) {
		this.client = (ISessionClient) networkObject;	
	}

	@Override
	public boolean isConnected(String[] ip) {
		return client.isConnected(ip);
	}

	@Override
	public void disconnect() {
		client.disconnect(true);	
	}

	@Override
	public void setKeepConnection(boolean keep) {
		client.setKeepConnection(keep);
	}

	@Override
	public boolean isKeepConnection() {
		return client.isKeepConnection();
	}

	@Override
	public void setClientView(IClientView view) {
		this.clientView = view;
		this.view = view;
		setRandomShip();
		clientView.setClientControler(this);
		int maxc = 180;
		int id1 = rand.nextInt(maxc);
		int id2 = rand.nextInt(maxc);
		int id3 = rand.nextInt(maxc);
		id = new int[3];
		id[0] = id1;
		id[1] = id2;
		id[2] = id3;
		clientView.setId(id);
	}
	
	public void setRandomShip() {
		int[] sizeShip = model.getSizeShips();
		List<IShip> positionUsed = new ArrayList<IShip>();
		Ship newShip;
		for (int i = 0; i < sizeShip.length; i++) {
			while (true) {
				newShip = new Ship(rand.nextInt(IConstants.NB_LINE_GRID) + 1, rand.nextInt(IConstants.NB_LINE_GRID) + 1, sizeShip[i], rand.nextBoolean(), clientView);
				if (shipIsCorrect(newShip, positionUsed)) {
					break;
				}
			}
			positionUsed.add(newShip);
		}
		model.setShips(positionUsed);
	}
	
	public boolean shipIsCorrect(IShip newShip, List<IShip> positionUsed) {
		if (!(0 < newShip.getX_init() && 0 < newShip.getY_init() && 
				newShip.getX_end() <= IConstants.NB_LINE_GRID + 1 && newShip.getY_end() <= IConstants.NB_LINE_GRID + 1)) {
			return false;
		}
		if (positionUsed == null) {
			return true;
		}
		for (IShip ship : positionUsed) {
			if (newShip.distanceBetween(ship) < 1) {
				return false;
			}
		}
		return true;
	}
	
	public int getPos_X_Grid_1(int x) {
		return (int) Math.floor(x / IConstants.SIZE_LINE_GRID);
	}
	
	public int getPos_Y_Grid_1(int y) {
		return (int) Math.floor(y / IConstants.SIZE_LINE_GRID);
	}
	
	public IShip getShipWithMousePosition(int x, int y) {
		x = getPos_X_Grid_1(x);
		y = getPos_Y_Grid_1(y);
		return getShipWithPosition(x, y);
	}

	public IShip getShipWithPosition(int x, int y) {
		for (IShip ship : model.getShips()) {
			if (ship.getX_init() <= x && x < ship.getX_end() && ship.getY_init() <= y && y < ship.getY_end()) {
				return ship;
			}
		}
		return null;
	}
	
	@Override
	public void dispose() {
		super.dispose();
		disconnect();
	}


	@Override
	public int[] getId() {
		return id;
	}

	@Override
	public void noServerAvailable() {
		//clientView.noServerAvailable();	
	}

	@Override
	public void notifySocketClosed() {
		view.notifyConnected(false);	
	}

	@Override
	public boolean searchForPlayer(String[] ip) {
		model.setGameInProgress(false);
		if (!isConnected(ip) && !connect(ip)) {
			return false;
		}
		model.setSearchForPlayer(true);
		cmd(IConstants.KW_SEARCHING_FOR_PLAYER);
		return true;
	}

	@Override
	public void receiveCmd(String cmd) {
		//log("Command receive : " + cmd);
		String[] req = cmd.split(" ");
		switch (req[0]) {
			case IConstants.KW_COMMAND_UNKNOW :
				break;
			case IConstants.KW_SEARCHING_FOR_PLAYER :
				log("Searching for a player...");
				break;
			case IConstants.KW_PLAYER_FOUND :
				playerFound();
				break;
			case IConstants.KW_YOUR_TURN :
				myTurn();
				break;
			case IConstants.KW_OTHER_PLAYER_TURN :
				waitTheOtherPlayer();
				break;
			case IConstants.KW_SHOOT :
				if (3 <= req.length) {
					takeShoot(Integer.parseInt(req[1]), Integer.parseInt(req[2]));
				}
				break;
			case IConstants.KW_MISSED :
				model.addPosMissed(new int[] {shoot_x, shoot_y});
				log ("You missed");
				break;
			case IConstants.KW_TOUCH :
				model.addPosTouch(new int[] {shoot_x, shoot_y});
				log ("You touch a ship");
				break;
			case IConstants.KW_SANK :
				sank();
				log ("You sank a ship");
				break;
			case IConstants.KW_YOU_WIN :
				log("Congratulation, you win !!");
				resetGame();
				break;
			case IConstants.KW_ABORD :
				log("The other player left the game !!");
				resetGame();
				break;
			case IConstants.KW_ERROR :
				log("Une erreur s'est produite");
				break;
		}
	}
	
	public void sank() {
		model.addPosTouch(new int[] {shoot_x, shoot_y});		
		int x_init = shoot_x;
		int x_end = shoot_x;
		int y_init = shoot_y;
		int y_end = shoot_y;
		boolean found = true;
		while (found) {
			found = false;
			for (int[] posT : model.getPosTouch()) {
				if (posT[1] == shoot_y) {
					if (x_init == posT[0] + 1) {
						x_init--;
						found = true;
					} else if (posT[0] - 1 == x_end) {
						x_end++;
						found = true;
					}
				} 
				if (posT[0] == shoot_x) {
					if (y_init == posT[1] + 1) {
						y_init--;
						found = true;
					} else if (posT[1] - 1 == y_end) {
						y_end++;
						found = true;
					}
				}
			}
		}
		x_end++;
		y_end++;
		model.addShipSank(new int[] {x_init, y_init, x_end, y_end});
	}
	
	public void abord() {
		if (model.gameIsInProgress()) {
			cmdInGame(IConstants.KW_ABORD);
			log("You left the game");
		}
		resetGame();
	}
	
	private void takeShoot(int x, int y) {
		log ("You took a shot at the pos " + x +" ; " + y);
		IShip shipTouch = getShipWithPosition(x, y);
		if (shipTouch == null) {
			cmdInGame(IConstants.KW_MISSED);
		} else {
			String cmd = shipTouch.touch(x, y);
			if (shipAllSank()) {
				cmdInGame(IConstants.KW_YOU_WIN);
			} else {
				cmdInGame(cmd);
			}
		}
	}

	private boolean shipAllSank() {
		for (IShip ship : model.getShips()) {
			if (!ship.sank()) {
				return false;
			}
		}
		log("All your ship has been sank, you loose");
		resetGame();
		return true;
	}

	private void resetGame() {
		model.resetModel();
		setRandomShip();
		clientView.reset();
	}

	private void cmdInGame(String cmd) {
		cmd(IConstants.KW_GAME_SESSION + " " + cmd);
	}

	private void playerFound() {
		log("Player found");
		model.setSearchForPlayer(false);
		model.setGameInProgress(true);
	}

	private void waitTheOtherPlayer() {
		log("It is the turn of the other player");
		model.setMyTurn(false);
	}

	private void myTurn() {
		log("Your turn, you can play");
		model.setMyTurn(true);
	}

	@Override
	public int getPosXEnemyShip(int mousex) {
		int x = (int) Math.floor(((double)(mousex - clientView.getX_InitGrid2())) / IConstants.SIZE_LINE_GRID) + 1;
		if (0 < x && x <= IConstants.NB_LINE_GRID) {
			return x;
		}
		return 0;
	}

	@Override
	public int getPosYEnemyShip(int mousey) {
		int y = (int) Math.floor(((double)(mousey - clientView.getY_InitGrid2())) / IConstants.SIZE_LINE_GRID);
		if (0 < y && y <= IConstants.NB_LINE_GRID) {
			return y;
		}
		return 0;
	}

	@Override
	public void shootAt(int x, int y) {	
		shoot_x = getPosXEnemyShip(x);
		shoot_y = getPosYEnemyShip(y);
		for (int[] posM : model.getPosMissed()) {
			if (posM[0] == shoot_x && posM[1] == shoot_y) {
				log("You already shot this position and you missed");
				return;
			}
		}
		for (int[] posT : model.getPosTouch()) {
			if (posT[0] == shoot_x && posT[1] == shoot_y) {
				log("You already shot this position and you touch a ship");
				return;
			}
		}
		if (shoot_x != 0 && shoot_y != 0) {
			model.setMyTurn(false);
			cmdInGame(IConstants.KW_SHOOT + " " + shoot_x + " " + shoot_y);
		}
	}
}
