package graphic_client;


import cs_2017_tp7_api.IApplication;
import cs_2017_tp7_api.IClientControler;
import cs_2017_tp7_api.IClientView;
import cs_2017_tp7_api.IModel;
import cs_2017_tp7_api.ISessionClient;
import cs_2017_tp7_common.Model;
import cs_2017_tp7_session_client.SessionClient;

public class ClientApplication implements IApplication {
	
	public static void main(String[] arg){
		ClientApplication app = new ClientApplication();
		app.run();
	}

	private void run(){
		IModel model = Model.getInstance();
		IClientControler controler = new ClientControler(this);
		controler.setModel(model);
		ISessionClient client = new SessionClient();
		controler.setNetwork(client);
		client.setClientControler(controler);
		IClientView view = new ClientView();
		controler.setClientView(view);
		controler.runClock();
		view.open();
	}
}
