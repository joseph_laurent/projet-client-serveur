package graphic_client;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import cs_2017_tp7_api.IClientControler;
import cs_2017_tp7_api.IClientView;
import cs_2017_tp7_api.IConstants;
import cs_2017_tp7_api.IControler;
import cs_2017_tp7_api.IShip;
import cs_2017_tp7_common.View;
import swing2swt.layout.BorderLayout;

public class ClientView extends View
		implements IClientView, PaintListener, ControlListener, MouseMoveListener, MouseListener, DisposeListener {

	protected Shell shell;
	private Canvas canvas;
	private Text txtCmd;
	private Text textPort;
	private Text textIp;
	private Button btnKeepconnection;
	private IClientControler controler;
	private Text txtId;
	private Composite composite_id;
	private Composite composite_4;
	private Button btnConnect;
	private Button btnDisconnect;
	private Button btnRefresh;
	private Button btnStartGame;
	private Button abord;
	private Text textLog;
	private Canvas baseCanvas;
	private int mousex;
	private int mousey;
	private boolean drag;
	private IShip shipSelected = null;
	private int d_x_cumul = 0;
	private int d_y_cumul = 0;
	private int x_initGrid2 = -1;
	private int y_initGrid2 = IConstants.SIZE_LINE_GRID;
	
	public int getX_InitGrid2() {
		return this.x_initGrid2;
	}
	
	public int getY_InitGrid2() {
		return this.y_initGrid2;
	}

	public void setClientControler(IClientControler controler) {
		this.controler = controler;
		model = controler.getModel();
	}

	public static void ma_in(String[] args) {
		try {
			ClientView window = new ClientView();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void initView(Canvas canvas) {
		super.initView();
		this.baseCanvas = canvas;
		textIp.setText(controler.getDefaultHost());
		textPort.setText(Integer.toString(controler.getDefaultPort()));
		btnKeepconnection.setSelection(((IClientControler) controler).isKeepConnection());
		btnConnect.setEnabled(true);
		btnDisconnect.setEnabled(false);
		if (IConstants.CONNECT_AT_STARTUP)
			connectIfNot();

	}

	@Override
	public void setId(int[] id) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				txtId.setText(id[0] + "." + id[1] + "." + id[2]);
				composite_id.setBackground(SWTResourceManager.getColor(id[0], id[1], id[2]));
			}
		});
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		initView(canvas);
		shell.addDisposeListener(this);
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			try {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			} catch (Exception e) {
			}
		}
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(600, 558);
		shell.setText("Client Application");
		shell.setLayout(new BorderLayout(0, 0));

		Composite composite_west = new Composite(shell, SWT.NONE);
		composite_west.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		composite_west.setLayoutData(BorderLayout.WEST);

		Composite composite_north = new Composite(shell, SWT.NONE);
		composite_north.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		composite_north.setLayoutData(BorderLayout.NORTH);
		composite_north.setLayout(new GridLayout(1, false));

		Composite composite_header = new Composite(composite_north, SWT.NONE);
		GridData gd_composite_header = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_composite_header.heightHint = 60;
		gd_composite_header.widthHint = 576;
		composite_header.setLayoutData(gd_composite_header);

		Composite composite = new Composite(composite_header, SWT.NONE);
		composite.setBounds(0, 27, 576, 28);

		btnRefresh = new Button(composite, SWT.NONE);
		btnRefresh.setBounds(286, 2, 100, 25);
		btnRefresh.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!model.gameIsInProgress()) {
					clearModel();
				}
			}
		});
		btnRefresh.setText("Set Random Ship");
		
		btnStartGame = new Button(composite, SWT.NONE);
		btnStartGame.setBounds(170, 2, 100, 25);
		btnStartGame.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnRefresh.setEnabled(false);
				String[] ip = new String[2];
				ip[0] = textIp.getText();
				ip[1] = textPort.getText();
				if (controler.searchForPlayer(ip)) {
					btnStartGame.setEnabled(false);
				}
			}
		});
		btnStartGame.setText("Start the game");
		
		abord = new Button(composite, SWT.NONE);
		abord.setBounds(402, 2, 80, 25);
		abord.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				controler.abord();
			}
		});
		abord.setText("Abord");

		Composite composite_1 = new Composite(composite_header, SWT.NONE);
		composite_1.setBounds(0, 0, 576, 28);

		textPort = new Text(composite_1, SWT.BORDER);
		textPort.setText("8051");
		textPort.setBounds(370, 3, 36, 21);

		textIp = new Text(composite_1, SWT.BORDER);
		textIp.setText("192.168.1.95");
		textIp.setBounds(276, 3, 87, 21);

		btnConnect = new Button(composite_1, SWT.NONE);
		btnConnect.setBounds(413, 1, 48, 25);
		btnConnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String[] ip = new String[2];
				ip[0] = textIp.getText();
				ip[1] = textPort.getText();
				controler.connect(ip);
			}
		});
		btnConnect.setText("connect");

		btnDisconnect = new Button(composite_1, SWT.NONE);
		btnDisconnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				controler.disconnect();
			}
		});
		btnDisconnect.setBounds(499, 1, 67, 25);
		btnDisconnect.setText("disconnect");

		btnKeepconnection = new Button(composite_1, SWT.CHECK);
		btnKeepconnection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				controler.setKeepConnection(btnKeepconnection.getSelection());
			}
		});
		btnKeepconnection.setSelection(true);
		btnKeepconnection.setBounds(161, 5, 108, 16);
		btnKeepconnection.setText("keepConnection");

		txtId = new Text(composite_1, SWT.BORDER);
		txtId.setBounds(78, 3, 76, 21);

		composite_id = new Composite(composite_1, SWT.NONE);
		composite_id.setLocation(7, 5);
		composite_id.setSize(64, 16);
		composite_id.setBackground(SWTResourceManager.getColor(238, 130, 238));

		composite_4 = new Composite(composite_1, SWT.NONE);

		composite_4.setBounds(471, 6, 22, 15);

		Composite composite_center = new Composite(shell, SWT.NONE);
		composite_center.setBackground(SWTResourceManager.getColor(SWT.COLOR_MAGENTA));
		composite_center.setLayoutData(BorderLayout.CENTER);
		composite_center.setLayout(new FillLayout(SWT.HORIZONTAL));

		canvas = new Canvas(composite_center, SWT.NONE);
		
		Composite composite_3 = new Composite(shell, SWT.NONE);
		composite_3.setLayoutData(BorderLayout.SOUTH);
		composite_3.setLayout(null);
		
		textLog = new Text(composite_3, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		textLog.setBounds(0, 0, 584, 144);

		canvas.addControlListener(this);
		canvas.addPaintListener(this);
		canvas.addMouseMoveListener(this);
		canvas.addMouseListener(this);
	}

	protected void showlog() {
	}

	private boolean connectIfNot() {
		String[] ip = new String[2];
		ip[0] = textIp.getText();
		ip[1] = textPort.getText();
		if (!controler.isConnected(ip))
			controler.connect(ip);
		return controler.isConnected(ip);
	}

	protected void sendCommand() {
		if (connectIfNot()) {
			String cmd = txtCmd.getText().trim();
			controler.cmd(cmd);
		} else
			log("unable to connect server " + textIp.getText() + ":" + textPort.getText());
	}

	protected void sendCommand(String cmd) {
		if (connectIfNot()) {
			txtCmd.setText(cmd);
			controler.cmd(cmd);
		} else
			log("unable to connect server " + textIp.getText() + ":" + textPort.getText());
	}
	
	/**
	 * Dessine la grille
	 * @param gc
	 */
	protected void paintReticule(GC gc) {
		Rectangle cb = baseCanvas.getBounds();
		gc.setForeground(colorgrid);
		
		int extraSpace = cb.width - (2 * IConstants.NB_LINE_GRID + 1) * IConstants.SIZE_LINE_GRID;
		int spaceBetweenGrid;
		if (0 < extraSpace) {
			spaceBetweenGrid = Math.min(5 * IConstants.SIZE_LINE_GRID, extraSpace);
		} else {
			spaceBetweenGrid = IConstants.SIZE_LINE_GRID;
		}
		
		int n_line = 1;
		//Draw of grid1
		int x_size = Math.min(((IConstants.NB_LINE_GRID + 1) * IConstants.SIZE_LINE_GRID), cb.width);
		int y_size = Math.min(((IConstants.NB_LINE_GRID + 1) * IConstants.SIZE_LINE_GRID), cb.height);
		for (int x = IConstants.SIZE_LINE_GRID; x <= x_size; x += IConstants.SIZE_LINE_GRID) {
			gc.drawLine(x, IConstants.SIZE_LINE_GRID, x, y_size);
			if (n_line <= IConstants.NB_LINE_GRID) {
				gc.drawText(String.valueOf((char) ((int) 'A' + n_line - 1)), x + (IConstants.SIZE_LINE_GRID / 2), (IConstants.SIZE_LINE_GRID / 2));
				n_line++;
			} else {
				gc.drawText(IConstants.TEXT_GRID_1, x_size* 2/5, y_size + (IConstants.SIZE_LINE_GRID / 2));
			}
		}
		
		n_line = 1;
		for (int y = IConstants.SIZE_LINE_GRID; y <= y_size; y += IConstants.SIZE_LINE_GRID) {
			gc.drawLine(IConstants.SIZE_LINE_GRID, y, x_size, y);
			if (n_line <= IConstants.NB_LINE_GRID) {
				gc.drawText(Integer.toString(n_line), (IConstants.SIZE_LINE_GRID / 2), y + (IConstants.SIZE_LINE_GRID / 2));
				n_line++;
			}
		}
		
		
		//Draw of grid2
		if (model.gameIsInProgress()) {
			x_initGrid2 = (IConstants.NB_LINE_GRID + 1) * IConstants.SIZE_LINE_GRID + spaceBetweenGrid;
			x_size = (2 * IConstants.NB_LINE_GRID + 1) * IConstants.SIZE_LINE_GRID + spaceBetweenGrid;
			if (x_initGrid2 < cb.width) {
				x_size = Math.min(x_size, cb.width);
				n_line = 1;
				for (int x = x_initGrid2; x <= x_size; x += IConstants.SIZE_LINE_GRID) {
					gc.drawLine(x, IConstants.SIZE_LINE_GRID, x, y_size);
					if (n_line <= IConstants.NB_LINE_GRID) {
						gc.drawText(String.valueOf((char) ((int) 'A' + n_line - 1)), x + (IConstants.SIZE_LINE_GRID / 2), (IConstants.SIZE_LINE_GRID / 2));
						n_line++;
					}
				}
				n_line = 1;
				for (int y = IConstants.SIZE_LINE_GRID; y <= y_size; y += IConstants.SIZE_LINE_GRID) {
					gc.drawLine(x_initGrid2, y, x_size, y);
					if (n_line <= IConstants.NB_LINE_GRID) {
						gc.drawText(Integer.toString(n_line), x_initGrid2 - (IConstants.SIZE_LINE_GRID / 2), y + (IConstants.SIZE_LINE_GRID / 2));
						n_line++;
					} else {
						gc.drawText(IConstants.TEXT_GRID_2, x_size* 17/24, y_size + (IConstants.SIZE_LINE_GRID / 2));
					}
				}
			}
		} else {
			x_initGrid2 = -1;
			y_initGrid2 = -1;
		}
	}
	
	protected void paintShip(GC gc) {
		if (model != null && model.getShips() != null) {
			synchronized (model) {
				gc.setLineWidth(3);
				for (IShip ship : model.getShips()) {
					gc.setForeground(new Color(Display.getDefault(), ship.getColor()[0], ship.getColor()[1], ship.getColor()[2]));
					gc.drawRectangle(ship.getX_init() * IConstants.SIZE_LINE_GRID, ship.getY_init() * IConstants.SIZE_LINE_GRID, (ship.isHorizontal() ? ship.getLength() : 1) * IConstants.SIZE_LINE_GRID, (ship.isHorizontal() ? 1 : ship.getLength()) * IConstants.SIZE_LINE_GRID);
					if (ship.wasTouch()) {
						for(int[] posTouch : ship.getPosTouch()) {
							gc.drawLine(posTouch[0] * IConstants.SIZE_LINE_GRID, posTouch[1] * IConstants.SIZE_LINE_GRID, (posTouch[0] + 1) * IConstants.SIZE_LINE_GRID, (posTouch[1] + 1) * IConstants.SIZE_LINE_GRID);
							gc.drawLine((posTouch[0] + 1) * IConstants.SIZE_LINE_GRID, posTouch[1] * IConstants.SIZE_LINE_GRID, posTouch[0] * IConstants.SIZE_LINE_GRID, (posTouch[1] + 1) * IConstants.SIZE_LINE_GRID);
						}
					}
				}
				gc.setLineWidth(1);
				gc.setForeground(colorblack);
			}
		}
	}
	
	protected void paintBomb(GC gc) {
		if (model != null) {
			gc.setLineWidth(10);
			gc.setForeground(new Color(Display.getDefault(), IConstants.COLOR_POS_MISSED[0], IConstants.COLOR_POS_MISSED[0], IConstants.COLOR_POS_MISSED[0]));
			for (int[] posM : model.getPosMissed()) {
				gc.drawOval((int)((posM[0] - 0.5) * IConstants.SIZE_LINE_GRID + getX_InitGrid2()), (int)((posM[1] + 0.5) * IConstants.SIZE_LINE_GRID + getY_InitGrid2()), 1, 1);
			}
			gc.setLineWidth(3);
			gc.setForeground(new Color(Display.getDefault(), IConstants.SHIP_COLOR_TOUCH[0], IConstants.SHIP_COLOR_TOUCH[1], IConstants.SHIP_COLOR_TOUCH[2]));
			for (int[] posT : model.getPosTouch()) {
				gc.drawLine((posT[0] - 1) * IConstants.SIZE_LINE_GRID + getX_InitGrid2(), posT[1] * IConstants.SIZE_LINE_GRID + getY_InitGrid2(), (posT[0]) * IConstants.SIZE_LINE_GRID + getX_InitGrid2(), (posT[1] + 1) * IConstants.SIZE_LINE_GRID + getY_InitGrid2());
				gc.drawLine((posT[0]) * IConstants.SIZE_LINE_GRID + getX_InitGrid2(), posT[1] * IConstants.SIZE_LINE_GRID + getY_InitGrid2(), (posT[0] - 1) * IConstants.SIZE_LINE_GRID + getX_InitGrid2(), (posT[1] + 1) * IConstants.SIZE_LINE_GRID + getY_InitGrid2());	
			}
			gc.setForeground(new Color(Display.getDefault(), IConstants.SHIP_COLOR_FLOW[0], IConstants.SHIP_COLOR_FLOW[1], IConstants.SHIP_COLOR_FLOW[2]));
			for (int[] posS : model.getShipSank()) {
				gc.drawRectangle((posS[0] - 1) * IConstants.SIZE_LINE_GRID + getX_InitGrid2(), posS[1] * IConstants.SIZE_LINE_GRID + getY_InitGrid2(), (posS[2] - posS[0]) * IConstants.SIZE_LINE_GRID, (posS[3] - posS[1]) * IConstants.SIZE_LINE_GRID);
			}
			gc.setLineWidth(1);
			gc.setForeground(colorblack);
		}
	}

	@Override
	public void paintControl(PaintEvent e) {
		paintReticule(e.gc);
		paintShip(e.gc);
		paintBomb(e.gc);
	}

	@Override
	protected Text getTxtLog() {
		return textLog;
	}

	@Override
	public void refresh() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				canvas.redraw();
			}
		});
	}

	@Override
	protected IControler getControler() {
		return controler;
	}
	
	public void clearModel() {
		synchronized (model) {
			controler.setRandomShip();
		}
		mousex = 0;
		mousey = 0;
		canvas.redraw();
	}

	public void mouseUp(MouseEvent e) {
		drag = false;
		d_x_cumul = 0;
		d_y_cumul = 0;
	}
	
	public void mouseDown(MouseEvent e) {
		drag = true;
		mousex = e.x;
		mousey = e.y;
		if (model.gameIsInProgress()) {
			
		} else {
			synchronized (model) {
				shipSelected = controler.getShipWithMousePosition(mousex, mousey);
			}
			baseCanvas.redraw();
		}
	}
	
	public void mouseMove(MouseEvent e) {
		if (drag && !model.gameIsInProgress() && shipSelected != null) {
			int d_x = controler.getPos_X_Grid_1(e.x) - controler.getPos_X_Grid_1(mousex) - d_x_cumul;
			int d_y = controler.getPos_Y_Grid_1(e.y) - controler.getPos_Y_Grid_1(mousey) - d_y_cumul;
			if (d_x != 0 || d_y != 0) {
				synchronized (model) {
					if (shipSelected.move(d_x, d_y, controler)) {
						d_x_cumul += d_x;
						d_y_cumul += d_y;
					}
				}
				baseCanvas.redraw(); //will redraw the whole model
			}
		}
	}
	

	public void mouseDoubleClick(MouseEvent e) {
		if (!model.gameIsInProgress() && shipSelected != null) {
			synchronized (model) {
				shipSelected.setHorizontal(!shipSelected.isHorizontal(), controler);
			}
			baseCanvas.redraw(); //will redraw the whole model
		}
		if (model.gameIsInProgress() && model.isMyTurn()) {
			controler.shootAt(e.x, e.y);
		}
	}

	public void controlMoved(ControlEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	public void controlResized(ControlEvent e) {
		IControler ctrl = getControler();
		if (ctrl != null)
			ctrl.setViewSize(baseCanvas.getClientArea());
	}
	
	public void refreshCanvas() {
		super.refreshCanvas();
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				baseCanvas.redraw();
			}
		});
	}

	@Override
	public void notifyConnected(boolean connected) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if (connected) {
					composite_4.setBackground(SWTResourceManager.getColor(255, 0, 0));
					btnConnect.setEnabled(false);
					btnDisconnect.setEnabled(true);
				} else {
					composite_4.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
					btnConnect.setEnabled(true);
					btnDisconnect.setEnabled(false);
				}
			}
		});
	}

	@Override
	public void reset() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				btnStartGame.setEnabled(true);
				btnRefresh.setEnabled(true);
			}
		});
		refreshCanvas();
	}
}
