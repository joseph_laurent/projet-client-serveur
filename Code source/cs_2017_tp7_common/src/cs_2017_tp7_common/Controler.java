package cs_2017_tp7_common;

import org.eclipse.swt.graphics.Rectangle;

import cs_2017_tp7_api.IApplication;
import cs_2017_tp7_api.IBaseView;
import cs_2017_tp7_api.IConstants;
import cs_2017_tp7_api.IControler;
import cs_2017_tp7_api.IModel;

public abstract class Controler implements IControler {

	protected IBaseView view;
	protected IModel model;

	private Rectangle viewArea = new Rectangle(10, 10, 10, 10);

	protected int localPort = IConstants.DEFAULT_PORT;
	protected String localHost = IConstants.DEFAULT_HOST;

	private Clock clock;

	protected abstract void disconnect();
	protected abstract int[] getId();
	

	@Override
	public abstract void setNetwork(Object networkpart);

	public String getDefaultHost() {
		return localHost;
	}

	public int getDefaultPort() {
		return localPort;
	}

	public Controler(IApplication application) {
	}

	@Override
	public void setViewSize(Object obj) {
		synchronized (viewArea) {
			Rectangle area = (Rectangle) obj;
			viewArea.x = area.x;
			viewArea.y = area.y;
			viewArea.width = area.width;
			viewArea.height = area.height;
		}
	}

	@Override
	public void runClock() {
		clock = new Clock("Controler");
		// wait the time for the view to be ready
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					return;
				}
				clock.start();
			}
		}).start();
	}


	@Override
	public void setModel(IModel model) {
		this.model = model;
	}

	@Override
	public IModel getModel() {
		return model;
	}

	/*-------------- Timer --------------------------------------------*/

	class Clock extends Thread {
	
		private boolean interrupted;

		public Clock(String id) {
			super();
			this.setName(id);
		}

		private synchronized void log(String m) {
			System.out.println(m);
		}

		@Override
		public void interrupt() {
			if (!isInterrupted()) {
				interrupted = true;
				log("is interrupted (1)");
				super.interrupt();
			}
		}

		@Override
		public boolean isInterrupted() {
			boolean result = super.isInterrupted();
			result = result || interrupted;
			return result;
		}

		void delay(int millis) {
			if (!interrupted && millis > 0)
				try {
					Thread.sleep(millis);
				} catch (InterruptedException e) {
					interrupted = true;
					log("is interrupted " + e.toString());
				}
		}

		public void run() {
		}
	}

	@Override
	public void addFigure(String figure) {
		view.refresh();
	}
	@Override
	public void log(String message) {
		view.log(message);
	}

	@Override
	public void log(Object clienttask, String message) {
		// Util.log( clienttask, message);
		log(message);
	}

	@Override
	public void dispose() {
		clock.interrupt();
		disconnect();
	}

	@Override
	public void notify(Object changed) {
		view.notify(changed);
	}

	public void setSocketError() {
		view.notifySocketError();

	}

	public void refreshView() {
		view.refreshCanvas();
	}

	public void clearView() {
	}

	public int[] parseArguments(String[] args) {
		int[] result = new int[args.length - 1];
		for (int i = 0; i < args.length - 1; i++) {
			result[i] = Integer.parseInt(args[i + 1]);
		}
		return result;
	}
}
