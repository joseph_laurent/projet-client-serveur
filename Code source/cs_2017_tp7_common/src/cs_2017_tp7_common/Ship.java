package cs_2017_tp7_common;

import java.util.ArrayList;
import java.util.List;

import cs_2017_tp7_api.IClientControler;
import cs_2017_tp7_api.IClientView;
import cs_2017_tp7_api.IConstants;
import cs_2017_tp7_api.IShip;

public class Ship implements IShip{
	private int x_init;
	private int y_init;
	private int x_end;
	private int y_end;
	private int length;
	private boolean isHorizontal;
	private int[] color;
	private final int TIME_COLOR = 500;
	private List<int[]> posTouch = new ArrayList<>();
	private IClientView view;
	
	public Ship(int x_init, int y_init, int length, boolean isHorizontal, IClientView view) {
		this.length = length;
		this.isHorizontal = isHorizontal;
		setPos(x_init, y_init);
		color = IConstants.SHIP_COLOR_DEFAULT;
		this.view = view;
	}
	
	public void setPos(int x_init, int y_init) {
		this.x_init = x_init;
		this.y_init = y_init;
		if (isHorizontal) {
			this.x_end = x_init + length;
			this.y_end = y_init + 1;
		} else {
			this.x_end = x_init + 1;
			this.y_end = y_init + length;
		}
	}
	
	public boolean wasTouch() {
		return (posTouch.size() != 0);
	}
	
	public int getX_init() {
		return x_init;
	}
	
	public int getY_init() {
		return y_init;
	}

	public boolean isHorizontal() {
		return isHorizontal;
	}
	
	public boolean sank() {
		return (posTouch.size() == length);
	}
	
	public boolean setHorizontal(boolean isHorizontal, IClientControler controler) {
		boolean oldIsHorizontal = this.isHorizontal;
		if (!controler.getModel().deleteShip(this)) {
			return false;
		}
		this.setHorizontalWhitoutControl(isHorizontal);
		if (controler.shipIsCorrect(this, controler.getModel().getShips())) {
			controler.getModel().addShip(this); 
			setColorTime(TIME_COLOR, IConstants.SHIP_COLOR_SUCESS);
			return true;
		}
		this.setHorizontalWhitoutControl(oldIsHorizontal);
		controler.getModel().addShip(this); 
		setColorTime(TIME_COLOR, IConstants.SHIP_COLOR_ERROR);
		return false;
	}
	
	private void setHorizontalWhitoutControl(boolean isHorizontal) {
		this.isHorizontal = isHorizontal;
		if (isHorizontal) {
			this.x_end = this.x_init + this.length;
			this.y_end = this.y_init + 1;
		} else {
			this.x_end = this.x_init + 1;
			this.y_end = this.y_init + this.length;
		}
	}

	@Override
	public String toString() {
		return "Ship [x_init=" + x_init + ", y_init=" + y_init + ", x_end=" + x_end + ", y_end=" + y_end + ", length="
				+ length + ", isHorizontal=" + isHorizontal + "]";
	}

	public int getX_end() {
		return x_end;
	}
	public int getY_end() {
		return y_end;
	}
	public int getLength() {
		return length;
	}

	@Override
	public double distanceBetween(IShip ship) {
		int d_x;
		int d_y;
		
		if (this.x_end < ship.getX_init()) {
			d_x = ship.getX_init() - this.x_end;
		} else if (ship.getX_end() < this.x_init) {
			d_x = this.x_init - ship.getX_end();
		} else {
			d_x = 0;
		}
		
		if (this.y_end < ship.getY_init()) {
			d_y = ship.getY_init() - this.y_end;
		} else if (ship.getY_end() < this.y_init) {
			d_y = this.y_init - ship.getY_end();
		} else {
			d_y = 0;
		}
		
		return Math.sqrt(d_x * d_x + d_y * d_y);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ship other = (Ship) obj;
		if (x_end != other.x_end)
			return false;
		if (x_init != other.x_init)
			return false;
		if (y_end != other.y_end)
			return false;
		if (y_init != other.y_init)
			return false;
		return true;
	}

	@Override
	public int[] getColor() {
		return this.color;
	}

	@Override
	public void setColor(int[] newColor) {
		this.color = newColor;
	}
	
	private void setColorTime(int delay, int[] newColor) {
		IShip thisShip = this;
		new Thread(new Runnable() {
			@Override
			public void run() {
				thisShip.setColor(newColor);
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				thisShip.setColor(IConstants.SHIP_COLOR_DEFAULT);
				view.refreshCanvas();
			}
		}).start();
	}

	@Override
	public boolean move(int d_x, int d_y, IClientControler controler) {
		int old_x_init = x_init;
		int old_y_init = y_init;
		if (!controler.getModel().deleteShip(this)) {
			return false;
		}
		this.setPos(x_init + d_x, y_init + d_y);
		if (controler.shipIsCorrect(this, controler.getModel().getShips())) {
			controler.getModel().addShip(this); 
			setColorTime(TIME_COLOR, IConstants.SHIP_COLOR_SUCESS);
			return true;
		}
		this.setPos(old_x_init, old_y_init);
		controler.getModel().addShip(this); 
		setColorTime(TIME_COLOR, IConstants.SHIP_COLOR_ERROR);
		return false;
	}

	@Override
	public String touch(int x_shoot, int y_shoot) {
		int[] shootPos = new int[] {x_shoot, y_shoot};
		boolean found = false;
		for (int[] pos : posTouch) {
			if (pos[0] == x_shoot && pos[1] == y_shoot) {
				found = true;
			}
		}
		if (!found) {
			posTouch.add(shootPos);
		}
		if (posTouch.size() == length) {
			color = IConstants.SHIP_COLOR_FLOW;
			return IConstants.KW_SANK;
		} else {
			color = IConstants.SHIP_COLOR_TOUCH;
			return IConstants.KW_TOUCH;
		}
	}
	

	@Override
	public List<int[]> getPosTouch() {
		return posTouch;
	}
}
