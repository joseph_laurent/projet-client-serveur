package cs_2017_tp7_common;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;

import cs_2017_tp7_api.IBaseView;
import cs_2017_tp7_api.IControler;
import cs_2017_tp7_api.IModel;

public abstract class View implements IBaseView {

	protected Color colorgrid;
	protected Color colorblack;
	protected Color colorred;
	protected IModel model;
	
	@Override
	public abstract void open();

	@Override
	public abstract void refresh();

	@Override
	public abstract void notifyConnected(boolean connected);

	protected abstract Text getTxtLog();

	protected abstract IControler getControler();

	protected void initView() {
		Display display = Display.getDefault();
		colorgrid = new Color(display, 158, 180, 190);
		colorblack = new Color(display, 0, 0, 0);
		colorred = new Color(display, 255, 0, 0);
	}

	@Override
	public void log(String mesg) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				getTxtLog().append(mesg + '\n');
			}
		});

	}

	public void widgetDisposed(DisposeEvent disposeEvent) {
		getControler().dispose();
	}

	@Override
	public void notify(Object changed) {
		IControler ctrl = getControler();
		if (ctrl != null)
			ctrl.notify(changed);
	}

	@Override
	public void notifySocketError() {
	}

	@Override
	public void refreshCanvas() {
	}

}
