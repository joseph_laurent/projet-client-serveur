package cs_2017_tp7_common;

import java.util.ArrayList;
import java.util.List;

import cs_2017_tp7_api.IModel;
import cs_2017_tp7_api.IShip;

public class Model implements IModel{
	private List<int[]> posTouch = new ArrayList<int[]>();
	private List<int[]> posMissed = new ArrayList<int[]>();
	private List<int[]> shipSank = new ArrayList<int[]>();
	private List<IShip> ships = null;
	private int[] shipsSize;
	private static Model instance;
	private boolean gameInProgress = false;
	private boolean searchForPlayer = false;
	private boolean myTurn = false;
	
	private Model() {
		shipsSize = new int[10];
		shipsSize[0] = 4;
		shipsSize[1] = 3;
		shipsSize[2] = 3;
		shipsSize[3] = 2;
		shipsSize[4] = 2;
		shipsSize[5] = 2;
		shipsSize[6] = 1;
		shipsSize[7] = 1;
		shipsSize[8] = 1;
		shipsSize[9] = 1;
	}
	
	public static Model getInstance() {
		if (instance == null)
			 instance = new Model();
		return instance;
	}
	
	public List<int[]> getShipSank() {
		return shipSank;
	}
	
	public void addShipSank(int[] pos) {
		shipSank.add(pos);
	}
	
	public List<int[]> getPosTouch() {
		return posTouch;
	}
	
	public void addPosTouch(int[] pos) {
		posTouch.add(pos);
	}
	
	public List<int[]> getPosMissed() {
		return posMissed;
	}
	
	public void addPosMissed(int[] pos) {
		posMissed.add(pos);
	}
	
	@Override
	public int[] getSizeShips() {
		return shipsSize;
	}
	
	public boolean gameIsInProgress() {
		return gameInProgress;
	}
	
	public boolean isSearchingForPlayer() {
		return this.searchForPlayer;
	}
	
	public void setGameInProgress(boolean gameInProgress) {
		this.gameInProgress = gameInProgress;
	}
	
	public void setSearchForPlayer(boolean searchForPlayer) {
		this.searchForPlayer = searchForPlayer;
	}

	@Override
	public void setShips(List<IShip> positionUsed) {
		this.ships = positionUsed;
	}

	@Override
	public List<IShip> getShips() {
		return this.ships;
	}

	@Override
	public void addShip(IShip newShip) {
		this.ships.add(newShip);
	}

	@Override
	public boolean deleteShip(IShip shipToDelete) {
		return this.ships.remove(shipToDelete);
	}

	@Override
	public void setMyTurn(boolean myTurn) {
		this.myTurn = myTurn;
	}

	@Override
	public boolean isMyTurn() {
		return this.myTurn;
	}

	@Override
	public void resetModel() {
		posTouch.clear();
		posMissed.clear();
		shipSank.clear();
		ships = null;
		gameInProgress = false;
		searchForPlayer = false;
		myTurn = false;
	}
}
