package cs_2017_tp7_threaded_server;

import cs_2017_tp7_api.IConstants;
import cs_2017_tp7_api.IGameSession;
import java.util.Random;

public class GameSession implements IGameSession {
	private ServiceTask player1;
	private ServiceTask player2;
	private Random rd = new Random();

	public GameSession(ServiceTask player1, ServiceTask player2) {
		this.player1 = player1;
		this.player2 = player2;
		player1.setGameSession(this);
		player2.setGameSession(this);
	}

	@Override
	public void start() {
		player1.send(IConstants.KW_PLAYER_FOUND);
		player2.send(IConstants.KW_PLAYER_FOUND);
		
		if (rd.nextBoolean()) {
			player1.send(IConstants.KW_YOUR_TURN);
			player2.send(IConstants.KW_OTHER_PLAYER_TURN);
		} else {
			player2.send(IConstants.KW_YOUR_TURN);
			player1.send(IConstants.KW_OTHER_PLAYER_TURN);
		}
	}
	
	public void request(int id, String[] req) {
		switch(req[1]) {
			case IConstants.KW_SHOOT:
				if (4 <= req.length) {
					sendCommandToOtherPlayer(id, IConstants.KW_SHOOT + " " + req[2] + " " + req[3]);
				} else {
					sendCommandToOtherPlayer(id, IConstants.KW_ERROR);
					sendCommandToPlayer(id, IConstants.KW_ERROR);
				}
				break;
			case IConstants.KW_MISSED:
				sendCommandToOtherPlayer(id, IConstants.KW_MISSED);
				sendCommandToPlayer(id, IConstants.KW_YOUR_TURN);
				sendCommandToOtherPlayer(id, IConstants.KW_OTHER_PLAYER_TURN);
				break;
			case IConstants.KW_TOUCH:
				sendCommandToOtherPlayer(id, IConstants.KW_TOUCH);
				sendCommandToOtherPlayer(id, IConstants.KW_YOUR_TURN);
				sendCommandToPlayer(id, IConstants.KW_OTHER_PLAYER_TURN);
				break;
			case IConstants.KW_SANK:
				sendCommandToOtherPlayer(id, IConstants.KW_SANK);
				sendCommandToOtherPlayer(id, IConstants.KW_YOUR_TURN);
				sendCommandToPlayer(id, IConstants.KW_OTHER_PLAYER_TURN);
				break;
			case IConstants.KW_YOU_WIN :
				sendCommandToOtherPlayer(id, IConstants.KW_YOU_WIN);
				break;
			case IConstants.KW_ABORD :
				sendCommandToOtherPlayer(id, IConstants.KW_ABORD);
				break;
			case IConstants.KW_ERROR:
				sendCommandToOtherPlayer(id, IConstants.KW_ERROR);
				sendCommandToPlayer(id, IConstants.KW_ERROR);
				break;
		}
	}
	
	private void sendCommandToPlayer(int id, String request) {
		if (player1.id == id) {
			player1.send(request);
		}
		if (player2.id == id) {
			player2.send(request);
		}
	}
	
	private void sendCommandToOtherPlayer(int id, String request) {
		if (player1.id == id) {
			player2.send(request);
		}
		if (player2.id == id) {
			player1.send(request);
		}
	}
}
