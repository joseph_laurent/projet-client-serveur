package cs_2017_tp7_threaded_server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;

import cs_2017_tp7_api.IConstants;
import cs_2017_tp7_api.IGameSession;
import cs_2017_tp7_api.IThreadedServer;

public class ServiceTask extends Thread {

	int id;
	int tic;
	boolean interrupted;
	private Socket socketService;
	private ServiceTask thistask = this;
	private IThreadedServer server;
	private SocketAddress remoteAddress;
	private PrintStream output;
	private int[] ident;
	private List<String> buffer = new ArrayList<String>();
	private IGameSession gameSession = null;

	public ServiceTask(int id, IThreadedServer threadedServer) {
		this.id = id;
		this.server = threadedServer;
	}

	@Override
	public void interrupt() {
		if (!isInterrupted()) {
			interrupted = true;
			Util.log(this, "task " + id + " is interrupted");
			super.interrupt();
		}
	}

	@Override
	public boolean isInterrupted() {
		boolean result = super.isInterrupted();
		result = result || interrupted;
		return result;
	}
	
	public void setGameSession(IGameSession gameSession) {
		this.gameSession = gameSession;
	}

	boolean action(String request) {
		server.logRequest(id, request);
		if (request.toLowerCase().contains("quit")) {
			output.println("stopping session");
			return false;
		} else {
			String[] req = request.split(" ");
			if (req[0].toLowerCase().equals("stop")) {
				output.println("the server will shutdown in 5 seconds");
				new Thread(new Runnable() {
					@Override
					public void run() {
						Util.delay(thistask, 5000);
						Util.globalEnd = true;
					}
				}).start();
			}
			if (req[0].equals(IConstants.KW_SEARCHING_FOR_PLAYER)) {
				server.startGame(this);
			} else if (req[0].equals(IConstants.KW_GAME_SESSION) && 2 <= req.length) {
				if (gameSession == null) {
					output.println(IConstants.KW_ERROR);
				} else {
					gameSession.request(this.id, req);
				}
			} else {
				output.println(IConstants.KW_COMMAND_UNKNOW);
			}
		}
		return true;
	}

	public String toString() {
		String result = "";
		result += id;
		result += " ";
		result += ident[0];
		result += ".";
		result += ident[1];
		result += ".";
		result += ident[2];
		result += remoteAddress;
		return result;
	}

	public void send(String data) {
		if (!socketService.isClosed())
			output.println(data);
	}

	public void prepare(String figure) {
		buffer.add(figure);
	}

	public void flush() {
		if (!socketService.isClosed())
			for (String figure : buffer) {
				output.println(figure);
			}
		buffer.clear();
	}

	@Override
	public void run() {
		Util.log(this, "task " + id + " starts");
		try {
			this.remoteAddress = socketService.getRemoteSocketAddress();
			String status = "le client " + id;
			status += " " + remoteAddress + " s'est connect�";
			Util.log(this, status);
			server.logStatus(this, id, status);
			// server.notify(this);
			output = new PrintStream(socketService.getOutputStream(), true); // autoflush
			BufferedReader networkIn = new BufferedReader(new InputStreamReader(socketService.getInputStream()));
			server.notify(this);
			while (!Util.globalEnd && !isInterrupted()) {
				String requeteclient = networkIn.readLine();
				if (requeteclient == null) {
					status = "le client " + id + " " + remoteAddress + " s'est d�connect�, fin de la session";
					Util.log(this, status);
					server.logStatus(this, id, status);
					break;
				} else {
					Util.log(this, "le client demande: " + (requeteclient == null ? "null" : requeteclient));
					if (!action(requeteclient))
						break;
				}
			}
		} catch (IOException e) {
			Util.log(this, "IO error in ServiceTask " + e.toString());
		} finally {
			try {
				socketService.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			Util.log(this, "arr�t de la session");
			server.logStatus(this, id, "session end");
		}
		server.endSession(this);
		Util.log(this, "session " + id + " ends");
	}

	public void setSocketService(Socket socketService) {
		this.socketService = socketService;
	}

	public void closeSocket() {
		try {
			if (socketService != null)
				socketService.close();
		} catch (IOException e) {

		}
	}

}