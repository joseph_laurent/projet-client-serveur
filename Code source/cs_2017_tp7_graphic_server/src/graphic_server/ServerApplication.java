package graphic_server;


import cs_2017_tp7_api.IApplication;
import cs_2017_tp7_api.IModel;
import cs_2017_tp7_api.IServerControler;
import cs_2017_tp7_api.IServerView;
import cs_2017_tp7_api.IThreadedServer;
import cs_2017_tp7_common.Model;
import cs_2017_tp7_threaded_server.ThreadedServer;

public class ServerApplication implements IApplication {

	
	public static void main(String[] arg){
		ServerApplication app = new ServerApplication();
		app.run();
	}

	private void run(){
		IModel model = Model.getInstance();
		IServerControler controler = new ServerControler(this);
		controler.setModel(model);
		IThreadedServer server = new ThreadedServer();
		controler.setNetwork(server);
		server.setServerControler(controler);
		IServerView view = new ServerView();
		controler.setServerView(view);
		controler.runClock();
		view.open();
	}
}
