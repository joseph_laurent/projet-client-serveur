package cs_2017_tp7_api;


public interface IControler {
	void setModel(IModel model);
	void setNetwork(Object networkpart);
	void setViewSize(Object obj);
	IModel getModel();
	void log(String message);
	void log(Object changed, String mesg);
	void dispose();
	void notify(Object changed);
	String getDefaultHost();
	int getDefaultPort();
	void setSocketError();
	void addFigure(String figure);
	int[] parseArguments(String[] args);
	void runClock();

}
