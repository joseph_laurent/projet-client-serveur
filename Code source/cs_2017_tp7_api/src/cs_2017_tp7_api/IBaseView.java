package cs_2017_tp7_api;

public interface IBaseView {
	void open();
	void log(String message);
	void refresh();
	void notifySocketError();
	void refreshCanvas();
	void notifyConnected(boolean connected);
	void notify(Object changed);
}
