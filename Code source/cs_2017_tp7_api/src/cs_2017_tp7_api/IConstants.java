package cs_2017_tp7_api;


/**
 * 
 * @author pfister - connecthive.com
 * initial api and implementation
 *
 */
public interface IConstants {
	static final boolean MODE_DEV = false; 
	static final int ID0 = 1;
    static final int ID1 = 2;
	static final int ID2 = 3;	
	static final int DEFAULT_PORT = 8051;
	static final String DEFAULT_HOST = "192.168.0.18";		
	static final int NB_LINE_GRID = 10;
	static final int SIZE_LINE_GRID = 32;
	static final String TEXT_GRID_1 = "Votre grille de jeu";
	static final String TEXT_GRID_2 = "La grille de jeu de votre adversaire";	
	static final boolean CONNECT_AT_STARTUP = true;
	static final int[] SHIP_COLOR_DEFAULT = new int[] {20,54,223};
	static final int[] SHIP_COLOR_TOUCH = new int[] {226,224,64};
	static final int[] SHIP_COLOR_FLOW = new int[] {137,12,12};
	static final int[] SHIP_COLOR_ERROR = new int[] {255,0,0};
	static final int[] SHIP_COLOR_SUCESS = new int[] {0,255,0};
	static final int[] COLOR_POS_MISSED = new int[] {100,100,100};
	//static final int[] COLOR_POS_TOUCH = new int[] {200,20,20};
	static final int TIME_OUT_TO_FIND_PLAYER = 20000;
	static final String KW_SEARCHING_FOR_PLAYER = "searching_player";
	static final String KW_PLAYER_FOUND = "Player_found";
	static final String KW_COMMAND_UNKNOW = "Command_unknow";
	static final String KW_YOUR_TURN = "Your_turn";
	static final String KW_OTHER_PLAYER_TURN = "Other_player_turn";
	static final String KW_GAME_SESSION = "Game_session";
	static final String KW_ERROR = "Error";
	static final String KW_SHOOT = "Shoot";
	static final String KW_TOUCH = "Touch";
	static final String KW_SANK = "Sank";
	static final String KW_MISSED = "Missed";
	static final String KW_YOU_WIN = "You_win";
	static final String KW_ABORD = "Abord";
}
