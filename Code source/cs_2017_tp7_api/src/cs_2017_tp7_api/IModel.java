package cs_2017_tp7_api;

import java.util.List;

public interface IModel {
	boolean gameIsInProgress();
	int[] getSizeShips();
	void setShips(List<IShip> positionUsed);
	List<IShip> getShips();
	void addShip(IShip newShip);
	boolean deleteShip(IShip shipToDelete);
	public boolean isSearchingForPlayer();
	public void setGameInProgress(boolean gameInProgress);
	public void setSearchForPlayer(boolean searchForPlayer);
	boolean isMyTurn();
	void setMyTurn(boolean myTurn);
	void addPosMissed(int[] pos);
	void addPosTouch(int[] pos);
	void addShipSank(int[] pos);
	List<int[]> getPosMissed();
	List<int[]> getPosTouch();
	List<int[]> getShipSank();
	void resetModel();
}
