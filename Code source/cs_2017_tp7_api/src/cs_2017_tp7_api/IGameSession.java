package cs_2017_tp7_api;

public interface IGameSession {
	void start();
	void request(int id, String[] req);
}
