package cs_2017_tp7_api;

public interface IClientView extends IBaseView {
	void setClientControler(IClientControler clientControler);
	void setId(int[] id);
	int getX_InitGrid2();
	int getY_InitGrid2();
	void reset();
}