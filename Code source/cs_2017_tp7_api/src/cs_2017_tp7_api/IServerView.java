package cs_2017_tp7_api;

public interface IServerView extends IBaseView {
	void setServerControler(IServerControler serverControler);
	void notifyId(String mesg);
	void setHost(String mac, String host, int port);
}
