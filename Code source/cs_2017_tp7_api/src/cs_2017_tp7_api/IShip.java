package cs_2017_tp7_api;

import java.util.List;

public interface IShip {
	public boolean wasTouch();
	int getY_end();
	int getX_end();
	int getX_init();
	int getY_init();
	boolean isHorizontal();
	int getLength();	
	double distanceBetween(IShip ship);
	boolean setHorizontal(boolean isHorizontal, IClientControler controler);
	boolean equals(Object obj);
	int[] getColor();
	void setColor(int[] newColor);
	boolean move(int d_x, int d_y, IClientControler controler);
	String touch(int x_shoot, int y_shoot);
	public List<int[]> getPosTouch();
	public boolean sank();
}
