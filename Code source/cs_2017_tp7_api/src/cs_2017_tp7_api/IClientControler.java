package cs_2017_tp7_api;

import java.util.List;

public interface IClientControler  extends IControler{
	boolean connect(String[] ip);
	void setClientView(IClientView view);
	void cmd(String cmd);
	boolean isConnected(String[] ip);
	void disconnect();
	void setKeepConnection(boolean keep);
	boolean isKeepConnection();
	void refreshView();
	void clearView();
	int[] getId();
	void noServerAvailable();
	void notifySocketClosed();
	void setRandomShip();
	IShip getShipWithMousePosition(int mousex, int mousey);
	boolean shipIsCorrect(IShip ship, List<IShip> ships);
	int getPos_X_Grid_1(int x);
	int getPos_Y_Grid_1(int y);
	boolean searchForPlayer(String[] ip);
	void receiveCmd(String cmd);
	int getPosXEnemyShip(int mousex);
	int getPosYEnemyShip(int mousey);
	void shootAt(int x, int y);
	void abord();
}
